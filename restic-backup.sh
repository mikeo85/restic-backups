#!/usr/bin/env bash
############################################################
#
#  RESTIC BACKUP SCRIPT
#  ---------------------------------------------------------
#  Simplifying the process of backups to multiple backends
#  using multiple profile options for various machines.
#
#        Written by: Mike Owens
#        Email:      mikeowens (at) fastmail (dot) com
#        Website:    https://michaelowens.me
#        GitLab:     https://gitlab.com/qu13t0ne
#        GitHub:     https://github.com/qu13t0ne
#        Mastodon:   https://infosec.exchange/@qu13t0ne
#        X (Twitter):https://x.com/qu13t0ne
#
#  To use this script, first set up at least one profile
#  and at least one target repo.
#  
############################################################

# //========== BEGIN INPUT VALIDATION ==========\\

RESTIC_BASE_DIR="$(dirname $BASH_SOURCE)"

## Input Validation Functions
## ----------------------------------------
exitHelp() { echo >&2 "$@";printf "%s" "$helpText";exit 1; }
countInputs() { [ "$#" -eq "$requiredNumOfInputs" ] || exitHelp "$requiredNumOfInputs argument(s) required, $# provided"; }

## Input Validation Process
## ----------------------------------------

### Define the Help Text With Usage Instructions
helpText="Usage: $(basename "$BASH_SOURCE") performs system backups using restic according to defined repos and profiles.
       $ $BASH_SOURCE <profile name> <repo name>
	Multiple profiles and repo targets can be used. List them separated by commas (no spaces).
		e.g. profile_A,profileB repo_1,repo_2
	The script will then back up all listed profiles to all listed repos.

"
### Define Input Requirements
### ----------------------------------------
#### Indicate the required number of inputs
requiredNumOfInputs=2

### Validate The Inputs
### ----------------------------------------
countInputs "$@" # Were the expected number of inputs provided?

profiles_string=$1
repos_string=$2

# Convert Inputs into Arrays
IFS=',' read -ra profiles <<< $profiles_string
IFS=',' read -ra repos <<< $repos_string

# Check for Profile configuration files
for profile in ${profiles[@]};do
	if ! [[ -f "$RESTIC_BASE_DIR/profiles/$profile.sh" &&  -f "$RESTIC_BASE_DIR/profiles/${profile}_exclude.txt" ]];then
		exitHelp "ERROR: $profile is not a properly configured profile"
	fi
done

# Check if Repo files are available
for repo in ${repos[@]};do
	if ! [ -f "$RESTIC_BASE_DIR/repos/$repo.sh" ]; then
		exitHelp "ERROR: $repo is not configured as a repository"
	fi
done

# \\=========== END INPUT VALIDATION ===========//

# //============================================\\
# \\============================================//

echo "Running $(basename "$BASH_SOURCE")..."
# //============ BEGIN SCRIPT BODY =============\\

# Load Env Settings
RESTIC_BASE_DIR="$(dirname $BASH_SOURCE)"
. $RESTIC_BASE_DIR/restic-env.sh

# FUNCTIONS
runBackup() {
	profiles_string=$1
	repos_string=$2

	# Convert Inputs into Arrays
	IFS=',' read -ra profiles <<< $profiles_string
	IFS=',' read -ra repos <<< $repos_string

	SECONDS=0
	declare -i ct=0
	((ct_jobs=${#profiles[@]}*${#repos[@]}))

	echo "-"
	echo "-"
	echo "RUNNING SCHEDULED BACKUP: $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")"
	echo "---------------------------"
	echo "PROFILES: ${profiles[*]}"
	echo "REPOS: ${repos[*]}"
	echo "TOTAL JOBS IN REQUEST: $ct_jobs"
	echo "-"

	# Loop through PROFILES
	for profile in ${profiles[@]};do

		# Load Profile Settings
		. "$RESTIC_BASE_DIR/profiles/$profile.sh"

		# Loop through repos and execute the backup process for each
		for repo in ${repos[@]};do

			ct+=1
			jobStart=$SECONDS

			RESTIC_PASSWORD_FILE=""

			# Load Repo Settings
			. "$RESTIC_BASE_DIR/repos/$repo.sh"

			# RUN BACKUP
			echo "STARTING JOB $ct OF $ct_jobs: $profile TO $repo"
			echo ""
			# restic backup $PROFILE_OPTIONS || echo "ERROR: $?"
			msg=$(restic backup $PROFILE_OPTIONS 2>&1)
			declare -i jobStatus=$?
			jobDuration=$(($SECONDS-$jobStart))
			if [ $jobStatus -eq 0 ];then
				echo -e "$msg"
				echo "JOB $ct OF $ct_jobs ($profile TO $repo) COMPLETE: $(elapsedTime $jobDuration)"
				# $RESTIC_BASE_DIR/send-email.sh status
			else
				errorMessage="ERROR CODE $jobStatus: $msg"
				echo $errorMessage
				$RESTIC_BASE_DIR/send-email.sh error $repo $profile "$errorMessage"
				echo "FAILED JOB $ct OF $ct_jobs ($profile TO $repo): $(elapsedTime $jobDuration)"
			fi

			echo "-"

		done
	done

	echo ""
	echo "FINISHED SCHEDULED BACKUP: $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")"
	echo "TOTAL BACKUP TIME: $(elapsedTime $SECONDS)"
	echo "---------------------------"
}

elapsedTime() { echo "$(($1 / 60)) minutes and $(($1 % 60)) seconds elapsed."; }

runBackup $profiles_string $repos_string >> $RESTIC_LOG_FILE 2>&1

# \\ ============= END SCRIPT BODY ============== //
echo "$(basename "$BASH_SOURCE") complete."

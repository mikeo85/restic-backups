#!/bin/bash
############################################################
# TEMPLATE PROFILE CONFIGURATION
############################################################

# ===== PROFILE INFO========================================
scriptName=$(basename "$BASH_SOURCE")
PROFILE_NAME=${scriptName%.*}

# ===== TAGS ===============================================
# Add additional tags separated by comma
TAGS="$HOSTNAME,${HOSTNAME}_$PROFILE_NAME"

# ===== INCLUDE ============================================
# List paths to include separated by spaces
RESTIC_INCLUDE_PATHS="$HOME"

# ===== EXCLUDE ============================================
# Global Exclusions File
GLOBAL_EXCLUDE_FILE="$RESTIC_BASE_DIR/exclude-global.txt"
# Profile Exclusions File
# *** FILE NAME OF EXCLUDE FILE MUST MATCH PROFILE NAME *** 
RESTIC_EXCLUDE_FILE="$RESTIC_BASE_DIR/profiles/${PROFILE_NAME}_exclude.txt"

# ===== BUILD PROFILE OPTIONS OUTPUT =======================
# Include a space a the end of each component
# Comment out components if not needed
PROFILE_OPTIONS="-xv "
PROFILE_OPTIONS+="--exclude-file $GLOBAL_EXCLUDE_FILE "
PROFILE_OPTIONS+="--exclude-file $RESTIC_EXCLUDE_FILE "
PROFILE_OPTIONS+="--tag $TAGS "
PROFILE_OPTIONS+="$RESTIC_INCLUDE_PATHS"
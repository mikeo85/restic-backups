<!-- markdownlint-disable MD013 MD036 MD040 MD041 -->
```
██████╗  █████╗  ██████╗██╗  ██╗    ██╗████████╗    ██╗   ██╗██████╗ 
██╔══██╗██╔══██╗██╔════╝██║ ██╔╝    ██║╚══██╔══╝    ██║   ██║██╔══██╗
██████╔╝███████║██║     █████╔╝     ██║   ██║       ██║   ██║██████╔╝
██╔══██╗██╔══██║██║     ██╔═██╗     ██║   ██║       ██║   ██║██╔═══╝ 
██████╔╝██║  ██║╚██████╗██║  ██╗    ██║   ██║       ╚██████╔╝██║     
╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝    ╚═╝   ╚═╝        ╚═════╝ ╚═╝     
```
<!-- Ascii source: https://textkool.com/en/ascii-art-generator?hl=default&vl=default&font=ANSI%20Shadow&text=Back%20It%20Up -->

# RESTIC BACKUPS

> Managing and automating system backups with [restic](https://restic.net/)

This repo contains a set of scripts and templates to simplify automation of system backups using Restic. It allows the user to configure multiple backup profiles and repositories, then use the same core script to execute backups for any combination of profiles and repositories, including sending multiple profiles to multiple repositories easily.

- Profiles: Data to be backed up + custom exclusions + custom tags + the ability to modify restic options
- Repositories: Locations to write backups + any repo-specific config

The intent of this project is to organize and simplify the process of using restic for backups without obfuscating the core restic functionality. Various other projects exist to help automate backups using restic, many of which are great and quite popular, but my experience was that they added layers of complexity and additional dependencies on top of restic that I didn't want.

I wanted to stay as close to the basics of restic as possible while creating repeatable templates for simiplicity and consistency. Because of this, any repository backends supported by restic can be used without a problem.

*Repo URL: [https://gitlab.com/qu13t0ne/restic](https://gitlab.com/qu13t0ne/restic)*

**Contents**

- [RESTIC BACKUPS](#restic-backups)
  - [Usage](#usage)
  - [Prerequisites / Dependencies](#prerequisites--dependencies)
  - [Installation and Setup](#installation-and-setup)
    - [Environment Variables](#environment-variables)
    - [Global Exclusions](#global-exclusions)
    - [Configure Profiles](#configure-profiles)
    - [Configure Repository Backends](#configure-repository-backends)
      - [Example Configuration: Local Storage](#example-configuration-local-storage)
      - [Example Configuration: Backblaze B2](#example-configuration-backblaze-b2)
  - [Backup Operations](#backup-operations)
    - [Performing Backups](#performing-backups)
    - [Scheduling Automated Backups](#scheduling-automated-backups)
      - [Schedule Backups with Cron](#schedule-backups-with-cron)
      - [Scheduling Status Emails with Cron](#scheduling-status-emails-with-cron)
    - [Managing Retention (Forget and Prune)](#managing-retention-forget-and-prune)
      - [Scheduling with Cron](#scheduling-with-cron)
  - [Logs](#logs)
  - [Additional Notes](#additional-notes)
    - [Troubleshooting](#troubleshooting)
    - ["Fatal: config cannot be loaded: unsupported repository version"](#fatal-config-cannot-be-loaded-unsupported-repository-version)
  - [Roadmap](#roadmap)
  - [Author](#author)
  - [Acknowledgments](#acknowledgments)
  - [See Also](#see-also)
  - [License](#license)

## Usage

1. Configure profile
2. Configure repo
3. run `restic-backup.sh myProfile myRepo`

## Prerequisites / Dependencies

- These scripts were designed and tested for Linux hosts.
  - They should function without issue on MacOS as well, but have not been tested.
  - Windows is an entirely different ballgame. Good luck.
- The target repositories should already be initialized according to the [restic docs](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html)

## Installation and Setup

- Install restic on the machine per [Restic Installation](https://restic.readthedocs.io/en/stable/020_installation.html#packages)
- (Optional but recommended) Configure MTA on device (msmtp, postfix, etc.) such that the `sendmail` command will work in the `send-email.sh` script.
  - If minimum solution needed, use msmpt. Example config in my dotfiles: [qu13t0ne/dotfiles/mail](https://gitlab.com/qu13t0ne/dotfiles/-/tree/main/mail)
- Clone this repo and `cd` into the repo directory

### Environment Variables

- Create a `restic-env.sh` from the template:

```sh
cp TEMPLATE_restic-env.sh restic-env.sh
```

- Set `RESTIC_ALERTS_EMAIL` as the email that should receive status and error alerts.

### Global Exclusions

- Review the file `exclude-global.txt` which contains exclusions to be applied across all profiles.
- Modify if desired.

### Configure Profiles

*All profile files must be in the `profiles/` directory.*

To configure a new backup profile:

- Copy the template files in the `profiles/` directory
  - The name you give the `.sh` file is the name of the profile & will be referenced when running the backup script later
  - The exclude `txt` file must be named with the same profile name as the matching `.sh` script plus `_exclude.txt`.
    - e.g.: Profile name 'MyProfile' requires `MyProfile.sh` & `MyProfile_exclude.txt`
  - *The `exclude` file **must** be created even if no paths will be excluded.* The backup script checks for both the `.sh` and `.txt` files.
- In the `.sh` file:
  - Edit the TAGS section.
    - By default, backups are tagged with the *hostname* and with *hostname_profilename*.
    - These can be modified/removed if desired, or additional tags can be added, separated by commas.
  - Edit the INCLUDE section to list all paths that should be included in the backup.
    - Paths should be separated by spaces within the double quotes.
    - If a path has spaces in it, enclose that path in single quotes.
  - Review the config in the EXCLUDE and BUILD sections. Edits should not be necessary in most cases, unless you want to make additional customizations to the default options.
- In the `_exclude.txt` file, list all paths or wildcards to excluded from the backup. This file is formatted according to standard restic spec, with one path/exclusion per line.

### Configure Repository Backends

*All repository backend files must be in the `repos/` directory.*

To configure a new repository backend:

- Copy the template file in the `repos/` directory
  - The name you give the `.sh` file is the name of the repo & will be referenced when running the backup script later
- Edit the newly-created file to add the necessary config info for your backend
  - At minimum, include the `RESTIC_REPOSITORY` and `RESTIC_PASSWORD` information as shown in the template
    - `RESTIC_REPOSITORY`: Syntax is as described in the [restic docs](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html)
    - `RESTIC_PASSWORD`: Password string
  - Additional options can be set according to the backend requirements in the [restic docs](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html)
- Run `chmod 700 *.sh` in the `repos/` directory to restrict access to these files to `-rwx------`, since they have credentials information in them.

#### Example Configuration: Local Storage

Local storage can include the local disk itself or any mounted file systems. Any remote file system must *already be mounted* before scripts in this repo can be run.

This example assumes a network file share has been mounted to the local machine at `/mnt/FileShare/` and a restic repo is initialized at `/mnt/FileShare/MyRepo`:

```sh
# Restic Variables
export RESTIC_REPOSITORY=/mnt/FileShare/MyRepo
export RESTIC_PASSWORD=MySecurePa$$word
```

#### Example Configuration: Backblaze B2

Backblaze B2 is an easy and inexpensive remote storage target. Follow [restic docs](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html#backblaze-b2) for setting up a repo on Backblaze B2.

```sh
# Restic Variables
export RESTIC_REPOSITORY=b2:bucketname:path/to/repo
export RESTIC_PASSWORD=MySecurePa$$word

# Backblaze B2 Config
export B2_ACCOUNT_ID=<MY_APPLICATION_KEY_ID>
export B2_ACCOUNT_KEY=<MY_APPLICATION_KEY>
export B2_CONNECTIONS=50
```

## Backup Operations

### Performing Backups

Use the script `restic-backup.sh` to actually perform the backups. The script performs system backups using restic according to defined repos and profiles.

```sh
/path/to/restic-backup.sh <profile name> <repo name>
```

Multiple profiles and repo targets can be used. List them separated by commas (no spaces), e.g.: `profile_A,profileB repo_1,repo_2`. The script will then back up all listed profiles to all listed repos:

- `Profile_A --> repo_1`
- `Profile_A --> repo_2`
- `Profile_B --> repo_1`
- `Profile_B --> repo_2`

### Scheduling Automated Backups

Backups should happen frequently and without you having to think about them. Schedule them!

#### Schedule Backups with Cron

- `crontab -e` to edit your cron jobs
- Insert something like the following example, which performs backups every 12 hours (6am & 6pm) and uses one profile and one repo target:

```sh
0 6,18 * * * /path/to/repo/restic-backup.sh Profile_A Repo_1 > /dev/null
```

- The `> /dev/null` at the end means that success will not trigger a cron email notification (if configured on your system), but that a job failure will trigger notification.

#### Scheduling Status Emails with Cron

- Adding the following to your crontab will send weekly backup job status emails on Sunday at noon. The status email will incude all log outputs since the previous status email.

```sh
0 12 * * Sun /path/to/repo/send-email.sh status > /dev/null
```

### Managing Retention (Forget and Prune)

Use the script `restic-forget-and-prune-by-policy.sh` to manage repositories according to the retention settings defined in the script. One or more repos should be given as script inputs.
> Policy: --keep-daily 30 --keep-weekly 8 --keep-monthly 12 --keep-yearly 100 --keep-last 4

#### Scheduling with Cron

- The script only needs to be added to **one (1)** computer per repository. Scheduling on more than one will just result in unnecessary duplication of checks.
- This only needs to be run once per week, at most
- Add something like this to your crontab to run the job every Sunday at noon:

```sh
0 12 * * Sun /path/to/repo/restic-forget-and-prune-by-policy.sh Repo_1 > /dev/null
```

## Logs

- All scripts will log their actions to `/path/to/repo/backup.log`

## Additional Notes

### Troubleshooting

### "Fatal: config cannot be loaded: unsupported repository version"

- Attempting to open a V2 repository with a version of restic that doesn't support it. Restic binary needs to be updated. Possibly multiple versions of restic are installed on the system, in which case the old version should be removed.
- Ref: [Unsupported Repository Version - Restic Forum](https://forum.restic.net/t/unsupported-repository-version/5574)

Confirm only one version of restic is installed:

```bash
IFS=: read -r -d '' -A path_array < <(printf '%s:\0' "$PATH")
for p in "${path_array[@]}"; do
  find $p -name restic
done
```

Update restic binary:

```bash
restic self-update
```

## Roadmap

- [x] Regular status emails
- [ ] SQL DB backup option

## Author

**Mike Owens**

- Website:  [michaelowens.me](https://michaelowens.me)
- GitHub:   [@qu13t0ne](https://github.com/qu13t0ne)
- GitLab:   [@qu13t0ne](https://gitlab.com/qu13t0ne)
- Mastodon: [@qu13t0ne@infosec.exchange](https://infosec.exchange/@qu13t0ne)
- X (Twitter):  [@quietmike8192](https://x.com/qu13t0ne)

## Acknowledgments

- [restic - Backups done right!](https://restic.net/)
- Heavily influenced by [linucksrox/restic-scripts](https://github.com/linucksrox/restic-scripts)

## See Also

- [qu13t0ne/airgap - Simple Offline Backups with Rsync](https://gitlab.com/qu13t0ne/airgap)

## License

**[MIT](LICENSE) © Mike Owens**

*About open source licenses: [ChooseALicense.com](https://choosealicense.com) / [Open Source Initiative](https://opensource.org/licenses) / [Developer's Guide](https://www.toptal.com/open-source/developers-guide-to-open-source-licenses)*

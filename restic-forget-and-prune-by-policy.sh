#!/usr/bin/env bash
set -euo pipefail
############################################################
#  RESTIC FORGET AND PRUNE BY POLICY
#  [[ By: Mike Owens (GitLab/GitHub @ qu13t0ne) ]]
#  ---------------------------------------------------------
#  Input pre-configured repo the apply policy to forget
#  and prune snapshots.
############################################################
# //========== BEGIN INPUT VALIDATION ==========\\

# Load Env Settings
RESTIC_BASE_DIR="$(dirname $BASH_SOURCE)"
. $RESTIC_BASE_DIR/restic-env.sh

## Input Validation Functions
## ----------------------------------------
exitHelp() { echo >&2 "$@";printf "%s" "$helpText";exit 1; }
countInputs() { [ "$#" -eq "$requiredNumOfInputs" ] || exitHelp "$requiredNumOfInputs argument(s) required, $# provided"; }

## Input Validation Process
## ----------------------------------------

### Define the Help Text With Usage Instructions
helpText="Usage: $(basename "$BASH_SOURCE") executes the forget and prune commands according to defined policy
       $ $BASH_SOURCE <repo name>
	Multiple repo targets can be used. List them separated by commas (no spaces).
		e.g. repo_1,repo_2
"
### Define Input Requirements
### ----------------------------------------
#### Indicate the required number of inputs
requiredNumOfInputs=1

### Validate The Inputs
### ----------------------------------------
countInputs "$@" # Were the expected number of inputs provided?

# Convert Inputs into Arrays
IFS=',' read -ra repos <<< $1

# Check if Repo files are available
for repo in ${repos[@]};do
	if ! [ -f "$RESTIC_BASE_DIR/repos/$repo.sh" ]; then
		exitHelp "ERROR: $repo is not configured as a repository"
	fi
done

# \\=========== END INPUT VALIDATION ===========//

# //============================================\\
# \\============================================//

echo "Running $(basename "$BASH_SOURCE")..."
# //============ BEGIN SCRIPT BODY =============\\

# Load Env Settings
. $RESTIC_BASE_DIR/restic-env.sh

# FUNCTIONS
forgetAndPrune() {
	# Convert Inputs into Arrays
	IFS=',' read -ra repos <<< $1

	SECONDS=0
	declare -i ct=0
	ct_jobs=${#repos[@]}

	echo "-"
	echo "-"
	echo "RUNNING FORGET AND PRUNE: $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")"
	echo "Forgetting and pruning snapshots according to retention policy"
	echo "---------------------------"
	echo "REPOS: ${repos[*]}"
	echo "TOTAL JOBS IN REQUEST: $ct_jobs"
	echo "-"

	# Loop through repos
	for repo in ${repos[@]};do
		ct+=1
		jobStart=$SECONDS

		RESTIC_PASSWORD_FILE=""

		# Load Repo Settings
		. "$RESTIC_BASE_DIR/repos/$repo.sh"

		# EXECUTE
		echo "STARTING JOB $ct OF $ct_jobs: $repo"
        echo "Policy: $RETENTION_POLICY"
		echo ""
		restic forget $RETENTION_POLICY --prune || echo "ERROR: $?"

		jobDuration=$(($SECONDS-$jobStart))
		echo "JOB $ct OF $ct_jobs ($repo) COMPLETE: $(elapsedTime $jobDuration)"
		echo "-"
	done
	echo "FINISHED FORGET AND PRUNE: $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")"
	echo "TOTAL TIME: $(elapsedTime $SECONDS)"
	echo "---------------------------"
}
elapsedTime() { echo "$(($1 / 60)) minutes and $(($1 % 60)) seconds elapsed."; }

# RUN FUNCTION
forgetAndPrune $1 >> $RESTIC_LOG_FILE 2>&1

# \\ ============= END SCRIPT BODY ============== //
echo "$(basename "$BASH_SOURCE") complete."

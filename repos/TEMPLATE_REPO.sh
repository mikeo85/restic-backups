#!/bin/bash
############################################################
# [TEMPLATE: ADD REPO NAME HERE FOR EASY REFERENCE]
############################################################

# Restic Variables
export RESTIC_REPOSITORY=/path/to/repo
export RESTIC_PASSWORD=MySecurePa$$word

# Retention Settings
## Example: --keep-daily 30 --keep-weekly 8 --keep-monthly 12 --keep-yearly 100 --keep-last 4
export RETENTION_POLICY="--keep-daily 30 --keep-weekly 8 --keep-monthly 12 --keep-yearly 100 --keep-last 4"

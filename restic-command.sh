#!/usr/bin/env bash
set -euo pipefail
############################################################
#  RESTIC COMMANDS WITH REPO REFERENCE
#  [[ By: Mike Owens (GitLab/GitHub @ qu13t0ne) ]]
#  ---------------------------------------------------------
#  This script lets you designate a pre-saved repo config
#  then run restic commands as they would normally be used.
#  Any restic command and command options can be included.
#
#  To use this script, run it and list the repo as the first
#  input, with any additional commands or inputs following.
#  At least one target repo must be configured.
############################################################

# //========== BEGIN INPUT VALIDATION ==========\\

## Input Validation Functions
## ----------------------------------------
exitHelp() { echo >&2 "$@";printf "%s" "$helpText";exit 1; }
countInputs() { [ "$#" -ge "$requiredNumOfInputs" ] || exitHelp "$requiredNumOfInputs argument(s) required, $# provided"; }

## Input Validation Process
## ----------------------------------------

### Define the Help Text With Usage Instructions
helpText="Usage: $(basename "$BASH_SOURCE") lets you designate a pre-configured repo before running additional restic commands.
       $ $BASH_SOURCE <repo name> <any additional commands and inputs>
"
### Define Input Requirements
### ----------------------------------------
#### Indicate the required number of inputs
requiredNumOfInputs=1

### Validate The Inputs
### ----------------------------------------
countInputs "$@" # Were the expected number of inputs provided?

repo=$1

# Check if Repo file is available
if ! [ -f "./repos/$repo.sh" ]; then
	exitHelp "ERROR: $repo is not configured as a repository"
fi

# \\=========== END INPUT VALIDATION ===========//

# //============================================\\
# \\============================================//

echo "Running $(basename "$BASH_SOURCE")..."
# //============ BEGIN SCRIPT BODY =============\\

RESTIC_PASSWORD_FILE=""

userInputs=($@)
unset userInputs[0]

# Load Env Settings
RESTIC_BASE_DIR="$(dirname $BASH_SOURCE)"
. $RESTIC_BASE_DIR/restic-env.sh

# Load Repo Settings
. "$RESTIC_BASE_DIR/repos/$repo.sh"

restic -r $RESTIC_REPOSITORY --password-command="echo $RESTIC_PASSWORD" ${userInputs[*]}

# \\ ============= END SCRIPT BODY ============== //
echo "$(basename "$BASH_SOURCE") complete."

#!/bin/bash
############################################################
# BASE ENVIRONMENT VARIABLES FOR RESTIC TO USE
############################################################

RESTIC_CACHE_DIR="$RESTIC_BASE_DIR/cache"
RESTIC_LOG_FILE="$RESTIC_BASE_DIR/backup.log"

# Email that notifications should be sent to
RESTIC_ALERTS_EMAIL=""

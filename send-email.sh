#!/usr/bin/env bash
set -euo pipefail
############################################################
#  SEND EMAIL NOTIFICATIONS 
#  [[ By: Mike Owens (GitLab/GitHub @ qu13t0ne) ]]
#  ---------------------------------------------------------
#  This script has options for sending emails with either:
#    - Summaries of backup job statuses, or
#    - Immediate alerts for job errors
#
#  Requires MTA on device (sendmail, postfix, etc.) such
#  that either the `sendmail` or `mail` command will work.
#    - If minimum solution needed, use msmpt.
#    - Example config in my dotfiles at 
#      https://gitlab.com/qu13t0ne/dotfiles/-/blob/main/msmtp/configuremsmtp.sh
############################################################

# //========== BEGIN INPUT VALIDATION ==========\\

## Input Validation Functions
## ----------------------------------------
exitHelp() { echo >&2 "$@";printf "%s" "$helpText";exit 1; }
countInputs() { [ "$#" -ge "$requiredNumOfInputs" ] || exitHelp "$requiredNumOfInputs argument(s) required, $# provided"; }
inList() { local ckInput="$1"; shift; local arr=("$@"); printf '%s\n' "${arr[@]}" | grep -P -q "^$ckInput\$" || exitHelp "\"$ckInput\" is not a valid argument."; }

## Input Validation Process
## ----------------------------------------

### Define the Help Text With Usage Instructions
helpText="Usage: $(basename "$BASH_SOURCE") sends either a summary notification or an immediate job error alert.
The two available inputs are 'status' and 'error'.
       $ $BASH_SOURCE <input>
For 'error', 3 additional inputs are required: <Repo> <Profile> <Error Message>
"
### Define Input Requirements
### ----------------------------------------
#### Indicate the required number of inputs
requiredNumOfInputs=1
##### List the valid inputs for each field.
##### Repeat for each field as needed. Remove if not required.
validInputs1=( 'status' 'error' )

### Validate The Inputs
### ----------------------------------------
countInputs "$@" # Were the expected number of inputs provided?
inList "$1" "${validInputs1[@]}" # check input 1

# \\=========== END INPUT VALIDATION ===========//

# //============================================\\
# \\============================================//

# echo "Running $(basename "$BASH_SOURCE")..."
# //============ BEGIN SCRIPT BODY =============\\

# Load Env Settings
RESTIC_BASE_DIR="$(dirname $BASH_SOURCE)"
. $RESTIC_BASE_DIR/restic-env.sh

if [ $1 == 'status' ];then
# PERIODIC STATUS EMAILS

	# Prepare Email Contents
	mySubject="Backup Status [$HOSTNAME] $(date +"%Y-%m-%d %A")"
	myFrontmatter="Backup status notification for $HOSTNAME.\nAll log entries since the last status notification are listed below."
	mySeparator="===================="
	myContent="$(awk '/^STATUS_NOTIFICATION_EMAIL/ {c=1; a="";next} {c=c+1;a=a$BASH_SOURCE"\n"}END{print a}' $RESTIC_LOG_FILE)"
	myEmail="Subject: $mySubject\n\n$myFrontmatter\n$mySeparator\n$myContent\n$mySeparator\n---eom---"

	# Send Email & Write to log file
    if command -v sendmail &> /dev/null
    then
	    echo -e "$myEmail" | sendmail $RESTIC_ALERTS_EMAIL && echo "STATUS_NOTIFICATION_EMAIL SENT $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")" >> $RESTIC_LOG_FILE
    else
	    echo -e "$myEmail" | mail $RESTIC_ALERTS_EMAIL && echo "STATUS_NOTIFICATION_EMAIL SENT $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")" >> $RESTIC_LOG_FILE
    fi

elif [ $1 == 'error' ];then
# ERROR ALERT EMAILS

	# Validate Inputs
	requiredNumOfInputs=4
	countInputs "$@" # Were the expected number of inputs provided?

	# Additional Inputs for Error Emails:
	REPO=$2
	PROFILE=$3
	MESSAGE=$4

	# Prepare Email Contents
	mySubject="BACKUP ERROR: $HOSTNAME | $REPO | $PROFILE | $(date +"%Y-%m-%d %A")"
	c1="A backup error occurred."
	c2="Host: $HOSTNAME"
	c3="Repo: $REPO"
	c4="Profile: $PROFILE"
	c5="----- Error message -----"
	c6=$MESSAGE
	myEmail="Subject: $mySubject\n\n$c1\n$c2\n$c3\n$c4\n$c5\n$c6\n--eom---"

	# Send Email & Write to log file
    if command -v sendmail &> /dev/null
    then
	    echo -e "$myEmail" | sendmail $RESTIC_ALERTS_EMAIL && echo "ERROR_NOTIFICATION_EMAIL SENT $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")"
    else
	    echo -e "$myEmail" | mail $RESTIC_ALERTS_EMAIL && echo "STATUS_NOTIFICATION_EMAIL SENT $(date +"%Y-%m-%d %T %Z (UTC%:::z) %A")" >> $RESTIC_LOG_FILE
    fi

else
	exitHelp "UNKNOWN CONDITION ENCOUNTERED"
fi

# \\ ============= END SCRIPT BODY ============== //
# echo "$(basename "$BASH_SOURCE") complete."
